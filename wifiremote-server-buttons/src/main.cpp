#include <ESP8266WiFi.h>
#include "ESPAsyncWebServer.h"

// Set your access point network credentials
const char* ssid = "ESP8266-Access-Point";
const char* password = "123456789";
#define led 2
#define in D1
#define out D2
struct station_info *stat_info;
struct ip_addr *IPaddress;
IPAddress address;
unsigned long previousMillis = 0;
const long interval = 400; 
unsigned long currentMillis = millis();
unsigned long lastUpdate = 0;


// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

String readTemp() {
  return String(digitalRead(in));
  //return String(1.8 * bme.readTemperature() + 32);
}

String readHumi() {
  return String(digitalRead(out));
}


void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);
  Serial.println();
  pinMode(in, INPUT_PULLUP);
  pinMode(out, INPUT_PULLUP);
  pinMode(led, OUTPUT);

  // Setting the ESP as an access point
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  server.on("/test", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", "test");

 //   request->send_P(200, "text/plain", String(digitalRead(out)).c_str());
  });
  server.on("/in", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(digitalRead(in)).c_str());
    Serial.print("in: ");

    Serial.println(digitalRead(in));
    Serial.println(millis());
   // previousMillis = currentMillis;
    lastUpdate=millis();

  });
  server.on("/out", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(digitalRead(out)).c_str());
  });

  
//  bool status;

  // default settings
  // (you can also pass in a Wire library object like &Wire2)

  
  // Start server
  server.begin();
}
 
void loop(){
  unsigned long currentMillis = millis();

  //esp_wifi_deauth_sta(0);
//  Serial.printf("Number of connected devices (stations) = %d\n", WiFi.softAPgetStationNum());
  //int a = WiFi.softAPgetStationNum();
  //Serial.println(WiFi.status());
  
  //a = a -1;
  //Serial.print("Stations connected = ");
  //Serial.println (a);

  if(currentMillis-lastUpdate>500){
    digitalWrite(led, HIGH);
  }
  else{
    digitalWrite(led, LOW);

  }
}